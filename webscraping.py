import requests
from bs4 import BeautifulSoup

url = 'https://www.rava.com/empresas/perfil.php?e=GGAL'
page = requests.get(url)

html = BeautifulSoup(page.content, 'html.parser')

tabla = html.find_all('table', class_='tablapanel')[-1]
balances = {}
rows = tabla.find_all('tr')[1:]
for row in rows:
    cols = row.find_all('td')
    balances[cols[0].get_text()] = {'Trimestre': cols[1].get_text(),
    'Capital': cols[2].get_text(),
    'Res. Trimestre': cols[3].get_text(),
    'Res. Ejercicio':cols[4].get_text(),
    'P.N': cols[5].get_text(),
    'V.L':cols[6].get_text(),
    'Per':cols[7].get_text()}

for key in balances.keys():
    print("{} {}".format(key, balances[key]))
